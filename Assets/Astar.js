#pragma strict

var targetPosition : Transform;

var range : int;
var tileSize : float = 1.0;

private var startTile : AStarNode;

private var targetTile : AStarNode;

private var openList = new Array(); //int[w*h];
private var closedList = new Array(); // int[w*h];
//private var cameFromList = new Array();

//private var gScore = new Array(); //cost from start along best known path;
//private var heuristicScore = new Array();
//private var estimatedTotalScore = new Array();

private var pathList = new Array();
private var nextPosition : Vector3;

function Start()
{	

}

function findPath(targetPosition : Vector3)
{
		//Calculate beginning tile
	var startTileX : int = Mathf.Floor(transform.position.x / tileSize);
	var startTileY : int = Mathf.Floor(transform.position.z / tileSize);
	startTile = new AStarNode(startTileX, startTileY, tileSize);
	
	//Calculate target tile
	var targetTileX : int = Mathf.Floor(targetPosition.x / tileSize);
	var targetTileY : int = Mathf.Floor(targetPosition.z / tileSize);
	targetTile = new AStarNode(targetTileX, targetTileY, tileSize);
	if(targetTile.isClosed || !isInBounds(targetTile))
		return new Array();
	
	openList.push(startTile);
	
	startTile.g = 0;
	startTile.h = heuristicFunction(startTile, targetTile);
	startTile.f = startTile.g + startTile.y;
	
	while(openList.length > 0)
	{
		var currentTile : AStarNode = openList.Pop();
		Debug.Log("Current X: "+currentTile.x+" Y: "+currentTile.y);
		if(currentTile.x == targetTile.x && currentTile.y == targetTile.y)
		{
			Debug.Log("Path: "+reconstructPath(currentTile).Join("; "));
			return reconstructPath(currentTile);
		}
			
		openList.remove(currentTile);
		currentTile.isClosed = true;
		closedList.push(currentTile);
		
		var neighbours = getNeighbours(currentTile);
		Debug.Log("Neighbours List: "+neighbours);
		for(var neighbour:AStarNode in neighbours)
		{
//			if(neighbour.isClosed)
//				continue;
			
			var tentativeGScore = currentTile.g + 
				Vector2.Distance(Vector2(currentTile.x, currentTile.y),	Vector2(neighbour.x, neighbour. y));
			
			var tentativeIsBetter = true;
			if(!neighbour.isOpen)
			{
				neighbour.isOpen = true;
				//we keep the openList array sorted so we can just pop the lowest cost node
				openList.push(neighbour);
				openList.sort(AStarNodeSort);
				
				neighbour.h = heuristicFunction(neighbour, targetTile);
			}
			else if(tentativeGScore < neighbour.g) {
				tentativeIsBetter = true;
			}
			else {
				tentativeIsBetter = false;
			}
			
			if(tentativeIsBetter)
			{
				neighbour.parent = currentTile;
				neighbour.g = tentativeGScore;
				neighbour.f = neighbour.g + neighbour.h;
				//f changes, so we update the list.
				openList.sort(AStarNodeSort);
			}
				
		}
	}
	
	return new Array();
}

function Update()
{
	if(Input.GetMouseButtonDown(0))
	{
		if(pathList.length == 0)
		{
			Debug.Log("Mouse Coords: "+Input.mousePosition);
			Debug.Log("Mouse World Coords: "+Camera.main.ScreenToWorldPoint(Input.mousePosition));
			pathList = findPath(Camera.main.ScreenToWorldPoint(
				Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y - transform.position.y)));
		}
	}
}

function FixedUpdate()
{
	if(Vector2.Distance(
		Vector2(transform.position.x, transform.position.z),
		Vector2(nextPosition.x, nextPosition.z)) 
		< 0.05)
	{
		if(pathList.length > 0)
		{
			var nextNode : AStarNode = pathList.Shift();
			nextPosition = 
				Vector3(nextNode.x * tileSize + tileSize/2, transform.position.y, nextNode.y * tileSize + tileSize/2);
				Debug.Log("Next Position: "+nextPosition);
			rigidbody.velocity = (nextPosition-transform.position).normalized * 2;
		}
		else {
			rigidbody.velocity = Vector3.zero;
		}
	}
	
}

//Using Manhattan method as a heuristic
function heuristicFunction(start : AStarNode, target : AStarNode)
{
	var cost : float = 1;
	return ((Mathf.Abs(start.x - target.x) + Mathf.Abs(start.y - target.y)) * cost);
}

function reconstructPath(currentNode : AStarNode) : Array
{
	var path = new Array();
	if(currentNode.parent)
	{
		path = reconstructPath(currentNode.parent);
		path.Push(currentNode);
	}
	else {
		path.Push(currentNode);
	}
	
	Debug.DrawLine(
		Vector3(currentNode.x * tileSize, 1.5, currentNode.y * tileSize), 
		Vector3(currentNode.x * tileSize + tileSize, 1.5, currentNode.y * tileSize),
		Color.yellow, 10000);
	Debug.DrawLine(
		Vector3(currentNode.x * tileSize + tileSize, 1.5, currentNode.y * tileSize),
		Vector3(currentNode.x * tileSize + tileSize, 1.5, currentNode.y * tileSize + tileSize),
		Color.yellow, 10000);
	Debug.DrawLine(
		Vector3(currentNode.x * tileSize + tileSize, 1.5, currentNode.y * tileSize + tileSize),
		Vector3(currentNode.x * tileSize, 1.5, currentNode.y * tileSize + tileSize),
		Color.yellow, 10000);
	Debug.DrawLine(
		Vector3(currentNode.x * tileSize, 1.5, currentNode.y * tileSize + tileSize),
		Vector3(currentNode.x * tileSize, 1.5, currentNode.y * tileSize),
		Color.yellow, 10000);

	return path;
}

function getNeighbours(node : AStarNode) : Array
{
	var neighbours = new Array();
	for(var i:int = -1; i < 2; ++i)
	{
		for(var j:int = -1; j < 2; ++j)
		{
			if(i == 0 && j == 0) continue;
			
			var newNode = new AStarNode(node.x + i, node.y + j, tileSize);
			if(isInBounds(newNode) && !isInClosedList(newNode))
			{
				newNode.parent = node;
				neighbours.Add(newNode);
			}
		}
	}
	
	return neighbours;
}

function isInBounds(node : AStarNode) : boolean
{
	return (node.x >= -range && node.x <= range && node.y >= -range && node.y <= range);
}

function isInClosedList(node : AStarNode)
{
	if(node.isClosed)
		return true;
		
	for(var closedNode : AStarNode in closedList)
	{
		if(node.x == closedNode.x && node.y == closedNode.y)
			return true;
	}
	
	return false;
}

/* We sort in a descending order, so when we Pop() we get
 * the node with the lowest cost
 */
function AStarNodeSort(node1 : AStarNode, node2 : AStarNode)
{
	return (node2.f - node1.f);
}

class AStarNode
{
	var x : int;
	var y : int;
	
	var f : float;
	var g : float;
	var h : float;
	
	var parent : AStarNode;
	
	var isOpen : boolean;
	var isClosed : boolean;
	var isTraversable : boolean;
	
	function AStarNode(x:int, y:int, tileSize:float)
	{
		this.x = x;
		this.y = y;
		
		isOpen = false;
		isClosed = false;
		
		checkIfTraversable(tileSize);
	}
	
	function checkIfTraversable(tileSize : float)
	{
		//should multiply position with half the tile size, radius should = tileSize/2
		var halfTileSize : float = tileSize / 2;
		Debug.DrawLine(
			Vector3(x * tileSize + halfTileSize, -1, y * tileSize + halfTileSize),
			Vector3(x * tileSize + halfTileSize, 2, y * tileSize + halfTileSize), Color.red, 1000); 
		//try Physics.CheckSphere
		if(Physics.CheckCapsule(Vector3(x * tileSize + halfTileSize, -1, y * tileSize + halfTileSize),
			Vector3(x * tileSize + halfTileSize, 2, y * tileSize + halfTileSize), 1))
		{
			isClosed = true;
			isTraversable = false;
		}
	}
	
}